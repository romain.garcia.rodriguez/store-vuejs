export interface Product {
  id?: number;
  title: string;
  description: string;
  price: number;
  image: string;
  category: string;
}

export interface Limit {
  min: number;
  max: number;
}
