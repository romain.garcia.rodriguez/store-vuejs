import { RouteConfig } from "vue-router";

import Home from "../views/Home.vue";
import EditProduct from "../views/EditProduct.vue";
import CreateProduct from "../views/CreateProduct.vue";

export const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/product/create",
    name: "create",
    component: CreateProduct,
  },
  {
    path: "/product/:id",
    name: "edit",
    component: EditProduct,
  },
];
